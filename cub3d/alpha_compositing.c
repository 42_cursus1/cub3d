/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "alpha_compositing.h"

static void	normalize_color(int *color, double *normed, bool way)
{
	int	i;

	i = 0;
	while (i < 3)
	{
		if (way)
			normed[i] = ((uint8_t *)color)[i] / 255.0;
		else
			((uint8_t *)color)[i] = normed[i] * 255;
		++i;
	}
}

static void	compose_pixel(double *bg, double *fg, double *tg, double alpha)
{
	int	i;

	i = 0;
	while (i < 3)
	{
		tg[i] = ((1.0 - alpha) * bg[i]) + (alpha * fg[i]);
		++i;
	}
}

void	alpha_composition(int *color, int background)
{
	double	bg[3];
	double	fg[3];
	double	tg[3];
	uint8_t	ialpha;
	double	alpha;

	ialpha = (*color) >> 24;
	ialpha ^= 0xff;
	alpha = ialpha / 255.0;
	normalize_color(&background, bg, true);
	normalize_color(color, fg, true);
	compose_pixel(bg, fg, tg, alpha);
	normalize_color(color, tg, false);
}
