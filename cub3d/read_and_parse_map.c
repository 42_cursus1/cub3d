/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "linelist.h"

static int	line_coherent(char *line, void *ptr, bool *spawnpoint)
{
	size_t		i;
	int			j;

	if (!ptr)
	{
		i = find_odd_one_out(line, " 1\n");
		if (line[i] || (count_char_occurences(line, '1') == 0))
			return (1);
		return (0);
	}
	i = find_odd_one_out(line, " 01NSEW\n");
	if (line[i])
		return (1);
	j = one_or_no_char_occurence(line, "NSEW");
	if (j == -1)
		return (1);
	if (j == 1)
	{
		if (*spawnpoint)
			return (1);
		*spawnpoint = true;
	}
	return (0);
}

static int	get_map_line(int fd, t_linelist **lnlst, bool *spawnpoint)
{
	char	*line;
	int		res;

	res = get_next_line(&line, fd);
	if (res <= 0)
		return (res);
	if (line[0] == '\n')
	{
		free(line);
		if (!(*lnlst))
			return (1);
		return (-1);
	}
	if (line_coherent(line, *lnlst, spawnpoint) || add_line(lnlst, line))
	{
		res = -1;
		free(line);
		return (-1);
	}
	return (res);
}

static int	form_map(t_map *map, t_linelist *lnlst)
{
	int			res;

	res = 0;
	map->number_of_lines = count_lines(lnlst);
	map->line_sizes = malloc(map->number_of_lines * sizeof(size_t));
	if (map->line_sizes)
	{
		map->cells = malloc((map->number_of_lines + 1) * sizeof(char *));
		if (map->cells)
			from_list_to_grid(map->cells, map->line_sizes, lnlst);
		else
		{
			res = 1;
			free(map->line_sizes);
		}
	}
	else
		res = 1;
	free_line_list(lnlst);
	return (res);
}

int	read_map(int fd, t_map *map)
{
	size_t		line_number;
	int			res;
	t_linelist	*lnlst;
	bool		spawnpoint;

	lnlst = NULL;
	res = 1;
	line_number = 0;
	spawnpoint = false;
	while (res > 0)
	{
		res = get_map_line(fd, &lnlst, &spawnpoint);
		if (res != 1)
			line_number++;
	}
	if (res >= 0 && spawnpoint)
		return (form_map(map, lnlst));
	free_line_list(lnlst);
	if (res < 0)
		printf("Error\nIncorrect map at line %zu\n", line_number);
	else
		printf("Error\nNo spawnpoint found in map\n");
	return (1);
}
