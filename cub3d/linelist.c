/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "linelist.h"

t_linelist	*alloc_linelist(char *line)
{
	t_linelist	*res;

	res = malloc(sizeof(t_linelist));
	if (res)
	{
		res->line = line;
		res->next = NULL;
	}
	return (res);
}

void	free_line_list(t_linelist *lnlst)
{
	t_linelist	*tmp;

	while (lnlst)
	{
		tmp = lnlst->next;
		free(lnlst->line);
		free(lnlst);
		lnlst = tmp;
	}
}

int	add_line(t_linelist **lnlst, char *line)
{
	t_linelist	*ln;
	t_linelist	*tmp;

	ln = alloc_linelist(line);
	if (!ln)
		return (1);
	if (!(*lnlst))
	{
		*lnlst = ln;
		return (0);
	}
	tmp = *lnlst;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = ln;
	return (0);
}

size_t	count_lines(t_linelist *lnlst)
{
	size_t		res;
	t_linelist	*tmp;

	res = 0;
	tmp = lnlst;
	while (tmp)
	{
		res++;
		tmp = tmp->next;
	}
	return (res);
}

void	from_list_to_grid(char **grid, size_t *x_sizes, t_linelist *lnlst)
{
	t_linelist	*tmp;
	char		*ptr;
	size_t		i;

	tmp = lnlst;
	i = 0;
	while (tmp)
	{
		grid[i] = tmp->line;
		tmp->line = NULL;
		tmp = tmp->next;
		ptr = find_chr(grid[i], '\n');
		*ptr = '\0';
		x_sizes[i] = ft_strlen(grid[i]);
		++i;
	}
	grid[i] = NULL;
}
