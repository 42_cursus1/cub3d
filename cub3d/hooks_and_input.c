/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "linux_keys.h"
#include "input.h"

static int	key_pressed(int keycode, t_input *input)
{
	if (keycode == K_W)
		*input |= INPUT_MOVE_FORWARD;
	else if (keycode == K_A)
		*input |= INPUT_MOVE_LEFT;
	else if (keycode == K_S)
		*input |= INPUT_MOVE_BACKWARD;
	else if (keycode == K_D)
		*input |= INPUT_MOVE_RIGHT;
	else if (keycode == K_AR_L)
		*input |= INPUT_ROTATE_LEFT;
	else if (keycode == K_AR_R)
		*input |= INPUT_ROTATE_RIGHT;
	else if (keycode == K_ESC)
		*input |= INPUT_ESC;
	else
		*input |= INPUT_OTHER;
	return (0);
}

static int	key_released(int keycode, t_input *input)
{
	if (keycode == K_W)
		*input &= (~INPUT_MOVE_FORWARD);
	else if (keycode == K_A)
		*input &= (~INPUT_MOVE_LEFT);
	else if (keycode == K_S)
		*input &= (~INPUT_MOVE_BACKWARD);
	else if (keycode == K_D)
		*input &= (~INPUT_MOVE_RIGHT);
	else if (keycode == K_AR_L)
		*input &= (~INPUT_ROTATE_LEFT);
	else if (keycode == K_AR_R)
		*input &= (~INPUT_ROTATE_RIGHT);
	else if (*input & INPUT_OTHER)
		*input &= (~INPUT_OTHER);
	return (0);
}

static int	close_with_cross(t_graph *graphics)
{
	mlx_loop_end(graphics->mlx_ptr);
	return (0);
}

void	hooks_setup(t_game *game)
{
	game->input = 0;
	mlx_hook(game->graphics.window, 2, 1L << 0, key_pressed, &(game->input));
	mlx_hook(game->graphics.window, 3, 1L << 1, key_released, &(game->input));
	mlx_hook(game->graphics.window, 33, 1L << 22, close_with_cross,
		&(game->graphics));
	mlx_loop_hook(game->graphics.mlx_ptr, cub3d, game);
}
