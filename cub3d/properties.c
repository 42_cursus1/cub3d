/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "parsing.h"

const char	*g_identifiers[] = {
	"F ",
	"C ",
	"NO ",
	"SO ",
	"EA ",
	"WE "
};

static int	identify_property(char *line)
{
	int		i;

	i = 0;
	while (i < 6)
	{
		if (!(mmcmp(g_identifiers[i], line, ft_strlen(g_identifiers[i]))))
			break ;
		i++;
	}
	if (i == 6)
		return (-1);
	return (i);
}

int	all_properties_set(t_graph *graphics)
{
	int	id;

	id = 0;
	while (id < 6)
	{
		if (id < 2)
		{
			if ((&(graphics->color_floor))[id] & (1 << 31))
				return (id);
		}
		else
		{
			if (!((char **)(&(graphics->texture_north)))[id - 2])
				return (id);
		}
		id++;
	}
	return (6);
}

static int	attribute_property(int id, char *line, t_graph *graphics)
{
	int	res;

	if (id < 2)
	{
		if ((&(graphics->color_floor))[id] & (1 << 31))
			res = attribute_color(id, line, graphics);
		else
			return (-1);
	}
	else
	{
		if (((char **)(&(graphics->texture_north)))[id - 2])
			return (-1);
		res = attribute_texture_path(id - 2, line, graphics);
	}
	if (res)
		return (-1);
	return (all_properties_set(graphics));
}

void	missing_property(t_graph *graphics)
{
	printf("Error\nMissing property '%s\b'\n",
		g_identifiers[all_properties_set(graphics)]);
}

int	parse_and_attribute(char *line, t_graph *graphics, size_t ln)
{
	int		res;
	size_t	i;

	res = identify_property(line);
	if (res == -1)
	{
		i = find_odd_one_out(line, " 1\n");
		if (!(line[i]))
			missing_property(graphics);
		else
			printf("Error\nUnknown property line %zu\n", ln);
	}
	else
	{
		res = attribute_property(res, line, graphics);
		if (res == -1)
			printf("Error\nIncorrect property line %zu\n", ln);
	}
	return (res);
}
