/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "parsing.h"

static int	open_config_file(char *path)
{
	int	fd;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		printf("Error\n'%s': %s\n", path, strerror(errno));
	return (fd);
}

static void	zero_em_all(t_map *map, t_graph *graphics, t_input *input)
{
	bzero((void *)map, sizeof(t_map));
	bzero((void *)graphics, sizeof(t_graph));
	graphics->color_floor = 0;
	graphics->color_ceiling = 0;
	graphics->color_floor ^= (1 << 31);
	graphics->color_ceiling ^= (1 << 31);
	*input = 0;
}

static int	splashscreen(t_graph *graphics)
{
	int		x;
	int		y;
	void	*image;

	x = 0;
	y = 0;
	image = mlx_xpm_file_to_image(graphics->mlx_ptr, "./splash.xpm", &x, &y);
	if (image && X_RESOLUTION > x && Y_RESOLUTION > y)
	{
		x = X_RESOLUTION / 2 - x / 2;
		y = Y_RESOLUTION / 2 - y / 2;
		if (x < 0)
			x = 0;
		if (y < 0)
			y = 0;
		mlx_put_image_to_window(graphics->mlx_ptr, graphics->window, image, x,
			y);
		mlx_destroy_image(graphics->mlx_ptr, image);
		return (0);
	}
	if (image)
		mlx_destroy_image(graphics->mlx_ptr, image);
	mlx_string_put(graphics->mlx_ptr, graphics->window, 0, 16, 0xffffff,
		"PRESS W/A/S/D/left/right TO START");
	return (1);
}

int	init_cub3d(char *path, t_map *map, t_graph *graphics, t_player *player)
{
	int		config_file_fd;
	int		res;
	t_input	input;

	config_file_fd = open_config_file(path);
	if (config_file_fd < 0)
		return (1);
	zero_em_all(map, graphics, &input);
	res = read_and_parse(config_file_fd, graphics, map);
	get_next_line(NULL, config_file_fd);
	close(config_file_fd);
	if (res || check_map(map, player) || init_minilibx(graphics)
		|| load_textures(graphics) || allocate_frame(graphics)
		|| opening_window(graphics))
	{
		free_map(map);
		free_graphics(graphics);
		return (1);
	}
	splashscreen(graphics);
	mlx_do_key_autorepeatoff(graphics->mlx_ptr);
	return (0);
}
