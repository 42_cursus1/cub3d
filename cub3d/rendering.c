/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "minimap.h"

static t_img	*select_texture(t_graph *graphics, t_player *player)
{
	if (player->side == 0 && player->map_x <= player->pos_x)
		return (graphics->texture_east);
	else if (player->side == 0 && player->map_x >= player->pos_x)
		return (graphics->texture_west);
	else if (player->side == 1 && player->map_y >= player->pos_y)
		return (graphics->texture_north);
	else if (player->side == 1 && player->map_y <= player->pos_y)
		return (graphics->texture_south);
	return (NULL);
}

int	render(t_map *map, t_graph *graphics, t_player *player)
{
	int		i;
	t_img	*current_texture;

	i = 0;
	while (i < X_RESOLUTION)
	{
		vector_init(player, i);
		stepper(player);
		dda(player, map);
		current_texture = select_texture(graphics, player);
		computing_ray_distance(player, current_texture->szy);
		draw_column(player, graphics, i, current_texture);
		i++;
	}
	if (CUB3D_BONUS)
		draw_minimap(graphics->frame, player, map);
	return (0);
}
