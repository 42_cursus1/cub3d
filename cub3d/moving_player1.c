/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "input.h"

const double	g_square_move = (double)SQUARE_2D / (double)X_RESOLUTION;

void	player_forward(t_player *player, t_map *map)
{
	double	x;
	double	y;

	x = player->pos_x + player->dir_x * g_square_move;
	y = player->pos_y + player->dir_y * g_square_move;
	if (!(CUB3D_BONUS) || get_cell_type(map, x, y) == SPACE)
	{
		player->pos_x = x;
		player->pos_y = y;
	}
}

void	player_backward(t_player *player, t_map *map)
{
	double	x;
	double	y;

	x = player->pos_x - player->dir_x * g_square_move;
	y = player->pos_y - player->dir_y * g_square_move;
	if (!(CUB3D_BONUS) || get_cell_type(map, x, y) == SPACE)
	{
		player->pos_x = x;
		player->pos_y = y;
	}
}

void	player_left(t_player *player, t_map *map)
{
	double	x;
	double	y;

	x = player->pos_x + player->dir_y * g_square_move;
	y = player->pos_y - player->dir_x * g_square_move;
	if (!(CUB3D_BONUS) || get_cell_type(map, x, y) == SPACE)
	{
		player->pos_x = x;
		player->pos_y = y;
	}
}

void	player_right(t_player *player, t_map *map)
{
	double	x;
	double	y;

	x = player->pos_x - player->dir_y * g_square_move;
	y = player->pos_y + player->dir_x * g_square_move;
	if (!(CUB3D_BONUS) || get_cell_type(map, x, y) == SPACE)
	{
		player->pos_x = x;
		player->pos_y = y;
	}
}

void	rotate_player(t_input input, t_player *player)
{
	double	olddir_x;
	double	oldplane_x;
	double	rotspeed;

	olddir_x = player->dir_x;
	oldplane_x = player->plane_x;
	if (input & INPUT_ROTATE_LEFT && !(input & INPUT_ROTATE_RIGHT))
		rotspeed = -0.03;
	else if (input & INPUT_ROTATE_RIGHT && !(input & INPUT_ROTATE_LEFT))
		rotspeed = 0.03;
	else
		return ;
	player->dir_x = player->dir_x * cos(rotspeed)
		- player->dir_y * sin(rotspeed);
	player->dir_y = olddir_x * sin(rotspeed)
		+ player->dir_y * cos(rotspeed);
	player->plane_x = player->plane_x * cos(rotspeed)
		- player->plane_y * sin(rotspeed);
	player->plane_y = oldplane_x * sin(rotspeed)
		+ player->plane_y * cos(rotspeed);
}
