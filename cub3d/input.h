/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#ifndef INPUT_H
# define INPUT_H

# define INPUT_MOVE_FORWARD 1
# define INPUT_MOVE_RIGHT 2
# define INPUT_MOVE_BACKWARD 4
# define INPUT_MOVE_LEFT 8

# define INPUT_ROTATE_LEFT 16
# define INPUT_ROTATE_RIGHT 32
# define INPUT_ESC 64
# define INPUT_OTHER 128

#endif
