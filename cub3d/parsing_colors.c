/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "colors.h"

static uint32_t	rgb_to_int(uint8_t red, uint8_t green, uint8_t blue)
{
	uint32_t	res;

	res = 0;
	((uint8_t *)&res)[0] = blue;
	((uint8_t *)&res)[1] = green;
	((uint8_t *)&res)[2] = red;
	return (res);
}

static char	*get_next_color(char *ptr)
{
	char	*res;

	res = find_chr(ptr, ',');
	if (*res != ',' || (res - ptr > 3) || (res == ptr))
		return (NULL);
	*res = '\0';
	++res;
	return (res);
}

static int	scolor_to_icolor(char *color_string, uint8_t *color_int)
{
	uint8_t		i;
	uint8_t		q;
	uint16_t	tmp;

	tmp = 0;
	*color_int = 0;
	i = ft_strlen(color_string);
	--i;
	q = 1;
	while (1)
	{
		if (color_string[i] < '0' || color_string[i] > '9')
			return (1);
		tmp += (color_string[i] - '0') * q;
		if (tmp > 255)
			return (1);
		if (i == 0)
			break ;
		i--;
		q *= 10;
	}
	*color_int = tmp;
	return (0);
}

static int	get_color_strings(char *line, char *color_strings[3])
{
	color_strings[RED] = &(line[2]);
	color_strings[RED] = skip_spaces(color_strings[RED]);
	if (!(*(color_strings[RED])))
		return (1);
	color_strings[GREEN] = find_chr(color_strings[RED], '\n');
	*(color_strings[GREEN]) = '\0';
	color_strings[GREEN] = find_chr(color_strings[RED], ' ');
	*(color_strings[GREEN]) = '\0';
	color_strings[GREEN] = get_next_color(color_strings[RED]);
	if (!color_strings[GREEN])
		return (1);
	color_strings[BLUE] = get_next_color(color_strings[GREEN]);
	if (!color_strings[BLUE])
		return (1);
	if (!(*(color_strings[BLUE])) || (ft_strlen(color_strings[BLUE]) > 3))
		return (1);
	return (0);
}

int	attribute_color(int id, char *line, t_graph *graphics)
{
	char	*color_strings[3];
	uint8_t	color_ints[3];
	int		i;

	if (get_color_strings(line, color_strings))
		return (1);
	i = -1;
	while (++i < 3)
	{
		if (scolor_to_icolor(color_strings[i], &(color_ints[i])))
			return (1);
	}
	(&(graphics->color_floor))[id] = rgb_to_int(color_ints[RED],
			color_ints[GREEN], color_ints[BLUE]);
	return (0);
}
