/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"

/*
 * steps to raycasting:
 *
 * 1. calculate initial vectors for ray position, plane and direction, 
 *    also, calculate length of ray from one x/y side to the next x/y side
 * 2. calculate step (up/down, left/right) and initial side distance
 * 3. while loop performs DDA
 * 4. calculate ray-wall distance, wall height,
 *    lowest and highest pixels of wall
*/

void	vector_init(t_player *player, int i)
{
	double	camera_x;

	camera_x = 2 * i / (double)X_RESOLUTION - 1;
	player->raydir_x = player->dir_x + player->plane_x * camera_x;
	player->raydir_y = player->dir_y + player->plane_y * camera_x;
	player->map_x = (unsigned int)player->pos_x;
	player->map_y = (unsigned int)player->pos_y;
	player->deltadist_x = fabs(1 / player->raydir_x);
	player->deltadist_y = fabs(1 / player->raydir_y);
}

void	stepper(t_player *player)
{
	if (player->raydir_x < 0)
	{
		player->step_x = -1;
		player->sidedist_x = (player->pos_x - player->map_x)
			* player->deltadist_x;
	}
	else
	{
		player->step_x = 1;
		player->sidedist_x = (player->map_x + 1.0 - player->pos_x)
			* player->deltadist_x;
	}
	if (player->raydir_y < 0)
	{
		player->step_y = -1;
		player->sidedist_y = (player->pos_y - player->map_y)
			* player->deltadist_y;
	}
	else
	{
		player->step_y = 1;
		player->sidedist_y = (player->map_y + 1.0 - player->pos_y)
			* player->deltadist_y;
	}
}

static void	ray_step(t_player *player)
{
	if (player->sidedist_x < player->sidedist_y)
	{
		player->sidedist_x += player->deltadist_x;
		player->map_x += player->step_x;
		player->side = 0;
	}
	else
	{
		player->sidedist_y += player->deltadist_y;
		player->map_y += player->step_y;
		player->side = 1;
	}
}

void	dda(t_player *player, t_map *map)
{
	int	hit;

	hit = 0;
	if (get_cell_type(map, (size_t)player->map_x,
			(size_t)player->map_y) != SPACE)
	{
		hit = 1;
		ray_step(player);
	}
	while (hit == 0)
	{
		ray_step(player);
		if (get_cell_type(map, (size_t)player->map_x,
				(size_t)player->map_y) != SPACE)
			hit = 1;
	}
	if (player->side == 0)
		player->perpwalldist = player->sidedist_x - player->deltadist_x;
	else
		player->perpwalldist = player->sidedist_y - player->deltadist_y;
}

/*
 * if line_height is bigger than screen height, then draw_start needs to be 0
 * (else, distortion occurs when the wall is bigger than the screen)
 */

void	computing_ray_distance(t_player *player, int tex_size)
{
	double	wall_hit;

	player->line_height = (int)(Y_RESOLUTION / player->perpwalldist);
	if (player->line_height > Y_RESOLUTION)
		player->draw_start = 0;
	else
		player->draw_start = (Y_RESOLUTION / 2) - (player->line_height / 2);
	player->draw_end = player->line_height / 2 + Y_RESOLUTION / 2;
	if (player->draw_end >= Y_RESOLUTION)
		player->draw_end = Y_RESOLUTION - 1;
	if (player->side == 0)
		wall_hit = player->pos_y + player->perpwalldist * player->raydir_y;
	else
		wall_hit = player->pos_x + player->perpwalldist * player->raydir_x;
	wall_hit -= floor(wall_hit);
	player->tex_x = (int)((1.0 - wall_hit) * (double)(tex_size));
	if (player->side == 0 && player->raydir_x > 0)
		player->tex_x = tex_size - player->tex_x - 1;
	if (player->side == 1 && player->raydir_y < 0)
		player->tex_x = tex_size - player->tex_x - 1;
	if (player->tex_x < 0)
		player->tex_x = 0;
	player->step = 1.0 * (tex_size - 1) / player->line_height;
	player->tex_pos = (player->draw_start - Y_RESOLUTION / 2
			+ player->line_height / 2) * player->step;
}
