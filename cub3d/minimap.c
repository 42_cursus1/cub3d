/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "minimap.h"

/*
 * dividing/multiplying/mod operations are somehow fucked
 * when done with preprocessor constants 🤷
 */

static const double		g_minimap_ray = MINIMAP_RAY;
static const double		g_minimap_size = MINIMAP_RAY * 2 + 1;
static const double		g_minimap_px_size = MINIMAP_PX_SIZE;
static const uint32_t	g_minimap_transparency = (128 << 24);
static const uint32_t	g_minimap_wall = 0x00646464 ^ g_minimap_transparency;
static const uint32_t	g_minimap_space = 0x00282828 ^ g_minimap_transparency;
static const uint32_t	g_minimap_player = 0x00ff0000 ^ g_minimap_transparency;

static int	is_around_player(t_player *player, double x, double y)
{
	return (x < player->pos_x + 0.3 && y < player->pos_y + 0.3
		&& x > player->pos_x - 0.3 && y > player->pos_y - 0.3);
}

void	draw_minimap(t_img *img, t_player *player, t_map *map)
{
	int		fx;
	int		fy;
	double	mx;
	double	my;

	my = player->pos_y - g_minimap_ray - 0.5;
	fy = 0;
	while (fy < g_minimap_px_size)
	{
		fx = 0;
		mx = player->pos_x - g_minimap_ray - 0.5;
		while (fx < g_minimap_px_size)
		{
			if (get_cell_type(map, floor(mx), floor(my)) == WALL)
				my_mlx_pixel_put(img, fx, fy, g_minimap_wall);
			else if (get_cell_type(map, floor(mx), floor(my)) == SPACE)
				my_mlx_pixel_put(img, fx, fy, g_minimap_space);
			if (is_around_player(player, mx, my))
				my_mlx_pixel_put(img, fx, fy, g_minimap_player);
			mx += g_minimap_size / g_minimap_px_size;
			fx++;
		}
		fy++;
		my += g_minimap_size / g_minimap_px_size;
	}
}
