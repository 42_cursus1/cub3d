/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"

const char	*g_cellchars = "01 ";
const char	*g_spawnchars = "NSEW";

t_celltype	get_cell_type(t_map *map, int x, int y)
{
	size_t	i;
	size_t	j;

	if (y >= 0 && y < (int)map->number_of_lines)
	{
		if (x >= 0 && x < (int)map->line_sizes[y])
		{
			i = 0;
			j = ft_strlen(g_cellchars);
			while (i < j)
			{
				if (map->cells[y][x] == g_cellchars[i])
					return ((t_celltype)i);
				++i;
			}
			return (SPACE);
		}
	}
	return (OUT);
}

static int	check_cell(t_map *map, int x, int y)
{
	if (get_cell_type(map, x, y) == SPACE
		&& (get_cell_type(map, x - 1, y) == OUT
			|| get_cell_type(map, x + 1, y) == OUT
			|| get_cell_type(map, x, y - 1) == OUT
			|| get_cell_type(map, x, y + 1) == OUT))
	{
		printf("Error\nMap not closed at y = %i, x = %i\n",
			y, x);
		return (1);
	}
	return (0);
}

/*
because of the bzero on init, all 4 vars are set to 0
-> plane_x and plane_y and dir_x and dir_y
*/

static void	set_spawnpoint(t_map *map, int x, int y, t_player *player)
{
	if (map->cells[y][x] == 'N')
	{
		player->plane_x = 0.96;
		player->dir_y = -1;
	}
	else if (map->cells[y][x] == 'S')
	{
		player->plane_x = -0.96;
		player->dir_y = 1;
	}
	else if (map->cells[y][x] == 'E')
	{
		player->plane_y = 0.96;
		player->dir_x = 1;
	}
	else if (map->cells[y][x] == 'W')
	{
		player->plane_y = -0.96;
		player->dir_x = -1;
	}
	else
		return ;
	player->pos_x = x + 0.5;
	player->pos_y = y + 0.5;
	map->cells[y][x] = '0';
}

int	check_map(t_map *map, t_player *player)
{
	int	y;
	int	x;

	y = 0;
	while (y < (int)map->number_of_lines)
	{
		x = 0;
		while (x < (int)map->line_sizes[y])
		{
			if (check_cell(map, x, y))
				return (1);
			set_spawnpoint(map, x, y, player);
			++x;
		}
		++y;
		if (x != 0)
			continue ;
		printf("Error\nEmpty line %i in map\n", y);
		return (1);
	}
	if (y != 0)
		return (0);
	printf("Error\nNo map detected\n");
	return (1);
}
