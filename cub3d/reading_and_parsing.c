/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "colors.h"
#include "parsing.h"

static int	rtnc_res(int res, int fd, t_graph *graphics)
{
	if (res > 0)
		return (0);
	get_next_line(NULL, fd);
	if (res == 0)
		missing_property(graphics);
	else if (errno)
		printf("Error\nCan't read file: %s\n", strerror(errno));
	return (1);
}

static int	read_textures_and_colors(int fd, t_graph *graphics)
{
	char	*line;
	size_t	line_number;
	int		res;

	res = 1;
	line_number = 0;
	while (res >= 0)
	{
		res = get_next_line(&line, fd);
		line_number++;
		if (res <= 0)
			break ;
		if (line[0] != '\n')
			res = parse_and_attribute(line, graphics, line_number);
		free(line);
		if (res == 6)
			break ;
	}
	return (rtnc_res(res, fd, graphics));
}

int	read_and_parse(int config_file_fd, t_graph *graphics, t_map *map)
{
	if (read_textures_and_colors(config_file_fd, graphics))
		return (1);
	if (read_map(config_file_fd, map))
		return (1);
	return (0);
}
