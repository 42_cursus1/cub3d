/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"

int	init_minilibx(t_graph *graphics)
{
	graphics->mlx_ptr = mlx_init();
	if (!graphics->mlx_ptr)
		return (1);
	return (0);
}

int	opening_window(t_graph *graphics)
{
	graphics->window = mlx_new_window(graphics->mlx_ptr, X_RESOLUTION,
			Y_RESOLUTION, "cub3d");
	if (!graphics->window)
		return (1);
	return (0);
}

int	allocate_frame(t_graph *graphics)
{
	void	*img_raw;
	t_img	*img;

	img_raw = mlx_new_image(graphics->mlx_ptr, X_RESOLUTION,
			Y_RESOLUTION);
	if (!img_raw)
		return (1);
	img = malloc(sizeof(t_img));
	if (!img)
	{
		mlx_destroy_image(graphics->mlx_ptr, img_raw);
		return (1);
	}
	mlx_img_to_img(img_raw, img);
	graphics->frame = img;
	return (0);
}

static void	*load_texture(void *mlx, char *path)
{
	void	*texture;
	t_img	*img;

	img = malloc(sizeof(t_img));
	printf("Loading texture '%s'...\n", path);
	texture = mlx_xpm_file_to_image(mlx, path, &(img->szx), &(img->szy));
	if (!texture || !img || (img->szx != img->szy))
	{
		if (texture)
			mlx_destroy_image(mlx, texture);
		if (img->szx == img->szy)
			printf("Error\nCould not load texture '%s'\n", path);
		else
			printf("Error\nTexture '%s' is not a square\n", path);
		free(img);
		img = NULL;
	}
	else
		printf("done.\n");
	free(path);
	mlx_img_to_img(texture, img);
	return (img);
}

int	load_textures(t_graph *graphics)
{
	size_t	i;
	bool	f;

	i = 0;
	f = false;
	while (i < 4)
	{
		if (!f)
		{
			(&(graphics->texture_north))[i] = load_texture(graphics->mlx_ptr,
				((char **)&(graphics->texture_north))[i]);
			if (!(&(graphics->texture_north))[i])
				f = true;
		}
		else
		{
			free((&(graphics->texture_north))[i]);
			(&(graphics->texture_north))[i] = NULL;
		}
		++i;
	}
	return (f);
}
