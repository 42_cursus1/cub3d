/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#include "../includes/cub3d.h"
#include "movement.h"

static const double	g_square_move = (double)SQUARE_2D / (double)X_RESOLUTION;

void	player_forward_left(t_player *player, t_map *map)
{
	double	x;
	double	y;
	double	z;

	z = 1 / sqrt(2);
	x = player->pos_x + (player->dir_x * z) * g_square_move;
	y = player->pos_y + (player->dir_y * z) * g_square_move;
	x += (player->dir_y * z) * g_square_move;
	y -= (player->dir_x * z) * g_square_move;
	if (!(CUB3D_BONUS) || get_cell_type(map, x, y) == SPACE)
	{
		player->pos_x = x;
		player->pos_y = y;
	}
}

void	player_forward_right(t_player *player, t_map *map)
{
	double	x;
	double	y;
	double	z;

	z = 1 / sqrt(2);
	x = player->pos_x + (player->dir_x * z) * g_square_move;
	y = player->pos_y + (player->dir_y * z) * g_square_move;
	x -= (player->dir_y * z) * g_square_move;
	y += (player->dir_x * z) * g_square_move;
	if (!(CUB3D_BONUS) || get_cell_type(map, x, y) == SPACE)
	{
		player->pos_x = x;
		player->pos_y = y;
	}
}

void	player_backward_left(t_player *player, t_map *map)
{
	double	x;
	double	y;
	double	z;

	z = 1 / sqrt(2);
	x = player->pos_x - (player->dir_x * z) * g_square_move;
	y = player->pos_y - (player->dir_y * z) * g_square_move;
	x += (player->dir_y * z) * g_square_move;
	y -= (player->dir_x * z) * g_square_move;
	if (!(CUB3D_BONUS) || get_cell_type(map, x, y) == SPACE)
	{
		player->pos_x = x;
		player->pos_y = y;
	}
}

void	player_backward_right(t_player *player, t_map *map)
{
	double	x;
	double	y;
	double	z;

	z = 1 / sqrt(2);
	x = player->pos_x - (player->dir_x * z) * g_square_move;
	y = player->pos_y - (player->dir_y * z) * g_square_move;
	x -= (player->dir_y * z) * g_square_move;
	y += (player->dir_x * z) * g_square_move;
	if (!(CUB3D_BONUS) || get_cell_type(map, x, y) == SPACE)
	{
		player->pos_x = x;
		player->pos_y = y;
	}
}

static void (*const	g_movements[])(t_player *, t_map *) = {
	player_forward,
	player_right,
	player_forward_right,
	player_backward,
	player_backward_left,
	player_backward_right,
	player_forward_left,
	player_left
};

void	move_player(t_input input, t_player *player, t_map *map)
{
	uint8_t	move;

	move = input & 15;
	if (move)
	{
		if (((move & 5) != 5) && ((move & 10) != 10))
		{
			if (move == 12)
				move = 5;
			else if (move == 9)
				move = 7;
			--move;
			(g_movements[move])(player, map);
			return ;
		}
	}
}
