DOS	=	cub3d \
		gnl \
		generic_utils \
		libmlx

LIBS	=	${DOS:%=./libs/%.a}


#FLAGS	:=  -Wall -Wextra -Werror -MMD
FLAGS	:=  -Wall -Wextra -Werror -g -MMD
#FLAGS	:=  -Wall -Wextra -Werror -g -MMD -fsanitize=address

MLXFLAGS	:= -lXext -lX11 -lm

COMPILO	:= gcc

NAME	:=	cub3D

all:	${NAME}
		@echo "\033[32m[Program is ready for use]\033[0m"

./libs/%.a: %
	- mkdir ./libs
	make -C $<
	diff $</$<.a $@ || cp $</$<.a $@

clean:
		make -C generic_utils clean
		make -C gnl clean
		make -C libmlx clean
		make -C cub3d clean
		@echo "\033[33m[Cleaned up]\033[0m"

fclean:		clean
		make -C generic_utils fclean
		make -C gnl fclean
		make -C cub3d fclean
		rm -f ${LIBS}
		rm -f ${NAME}
		@echo "\033[33m[Fully cleaned up]\033[0m"

${NAME}: ${LIBS}
	${COMPILO} ${FLAGS} -o ${NAME} ${LIBS} ${MLXFLAGS}
	@echo "\033[32m[cub_3D created]\033[0m"

bonus:
	make -C cub3d bonus
	make all

cub:
	make -C cub3d re
	make all

-include ${DEP}


re:		fclean all

.PHONY:		all clean fclean re dependencies cub bonus
