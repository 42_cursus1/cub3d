/* ************************************************************************** */
/**//**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//**/
/* ************************************************************************** */

#include "../includes/gnl.h"

static t_rbpf	*alloc_remain(int fd)
{
	t_rbpf	*res;

	res = malloc(sizeof(t_rbpf));
	if (!res)
		return (NULL);
	res->fd = fd;
	res->remaining_bytes[0] = '\0';
	res->next = NULL;
	return (res);
}

static void	insert_remain(t_rbpf **remains, t_rbpf *added_remain)
{
	t_rbpf	*a;
	t_rbpf	*b;

	if (!(*remains) || ((*remains)->fd > added_remain->fd))
	{
		a = *remains;
		*remains = added_remain;
		added_remain->next = a;
		return ;
	}
	a = *remains;
	b = (*remains)->next;
	while (a)
	{
		if ((a->fd < added_remain->fd) && (!b || (added_remain->fd < b->fd)))
		{
			added_remain->next = b;
			a->next = added_remain;
			break ;
		}
		a = b;
		b = b->next;
	}
}

char	*get_remains(t_rbpf **remains, int fd)
{
	t_rbpf	*added_remain;
	t_rbpf	*remain;

	remain = *remains;
	while (remain)
	{
		if (remain->fd == fd)
			break ;
		remain = remain->next;
	}
	if (remain)
		return (remain->remaining_bytes);
	added_remain = alloc_remain(fd);
	if (!added_remain)
		return (NULL);
	insert_remain(remains, added_remain);
	return (added_remain->remaining_bytes);
}

void	remove_fd_remain(t_rbpf **remains, int fd)
{
	t_rbpf	*a;
	t_rbpf	*b;

	if (!(*remains))
		return ;
	if ((*remains)->fd == fd)
	{
		a = *remains;
		*remains = a->next;
		free(a);
		return ;
	}
	a = *remains;
	b = (*remains)->next;
	while (b)
	{
		if (fd == b->fd)
		{
			a->next = b->next;
			free(b);
			break ;
		}
		a = b;
		b = b->next;
	}
}

void	free_all_remains(t_rbpf **remains)
{
	t_rbpf	*a;
	t_rbpf	*b;

	a = *remains;
	while (a)
	{
		b = a->next;
		free(a);
		a = b;
	}
	*remains = NULL;
}
