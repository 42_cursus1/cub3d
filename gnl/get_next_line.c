/* ************************************************************************** */
/**//**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//**/
/* ************************************************************************** */

#include "../includes/gnl.h"

static char	*extract_remaining_line(char *buffer)
{
	char		*tmp;
	char		*res;
	size_t		size;

	tmp = find_chr(buffer, '\n');
	if (!tmp)
		return (NULL);
	size = tmp - buffer + 1;
	if (*tmp)
		size++;
	res = malloc(size);
	if (!res)
		return (NULL);
	res[size - 1] = 0;
	mmcpy(buffer, res, tmp - buffer + 1);
	if (*tmp == '\n')
	{
		tmp++;
		ft_strcpy(tmp, buffer);
	}
	else
		buffer[0] = '\0';
	return (res);
}

static long int	line_len(char *line)
{
	long int	size;

	size = 0;
	while (line[size])
		size++;
	return (size);
}

static void	add_line(char *buffer, char **line, char *remains)
{
	char	*tmp;
	char	*res;
	size_t	size;

	tmp = find_chr(buffer, '\n');
	if (tmp)
	{
		size = tmp - buffer + 1 + ft_strlen(*line);
		if (*tmp == '\n')
			size++;
		res = malloc(size);
		if (!res)
			return ;
		*res = '\0';
		ft_strcpy(*line, res);
		free(*line);
		mmcpy(buffer, find_chr(res, '\0'), tmp - buffer + 1);
		res[size - 1] = '\0';
		if (*tmp == '\n')
		{
			++tmp;
			ft_strcpy(tmp, remains);
		}
		*line = res;
	}
}

static long int	gnl(int fd, char *current_remains, char **line)
{
	long int	res;
	char		buffer[GNL_BUFFER_SIZE + 1];
	char		*tmp;

	tmp = current_remains;
	while (tmp)
	{
		if (current_remains[0])
			*line = extract_remaining_line(current_remains);
		else
		{
			res = read(fd, buffer, GNL_BUFFER_SIZE);
			if (res <= 0)
				return (res);
			buffer[res] = '\0';
			add_line(buffer, line, current_remains);
		}
		tmp = find_chr(*line, '\n');
		if (tmp && (*tmp == '\n'))
			return (line_len(*line));
	}
	return (-1);
}

long int	get_next_line(char **line, int fd)
{
	static t_rbpf	*remains = NULL;
	char			*current_remains;
	long int		res;

	if (!line)
	{
		if (fd < 0)
			free_all_remains(&remains);
		else
			remove_fd_remain(&remains, fd);
		return (0);
	}
	*line = NULL;
	current_remains = get_remains(&remains, fd);
	if (!current_remains)
		return (-2);
	res = gnl(fd, current_remains, line);
	if (res <= 0 && !(*current_remains))
		remove_fd_remain(&remains, fd);
	return (res);
}
