/* ************************************************************************** */
/**//**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//**/
/* ************************************************************************** */

#include "../includes/generic_utils.h"

size_t	find_odd_one_out(const char *s, const char *charset)
{
	size_t		i;
	const char	*tmp;

	i = 0;
	while (s[i])
	{
		tmp = charset;
		while (*tmp && *tmp != s[i])
			++tmp;
		if (!(*tmp))
			break ;
		++i;
	}
	return (i);
}

size_t	count_char_occurences(const char *s, const char c)
{
	size_t	res;

	res = 0;
	while (*s)
	{
		if (*s == c)
			++res;
		++s;
	}
	return (res);
}

int	one_or_no_char_occurence(const char *s, const char *charset)
{
	size_t	i;
	bool	f;

	f = false;
	while (*s)
	{
		i = 0;
		while (charset[i] && charset[i] != *s)
			++i;
		if (charset[i])
		{
			if (f)
				return (-1);
			f = true;
		}
		++s;
	}
	return (f);
}
