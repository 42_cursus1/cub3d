/* ************************************************************************** */
/**//**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//**/
/* ************************************************************************** */

#include "../includes/generic_utils.h"

int	ft_strcmp(const char *a, const char *b)
{
	while (*a && *b && (*a == *b))
	{
		a++;
		b++;
	}
	return (!(*a == *b));
}

char	*skip_spaces(char *ptr)
{
	while (*ptr && (*ptr == ' '))
		ptr++;
	return (ptr);
}

int	mmcmp(const void *a, const void *b, size_t n)
{
	size_t	i;

	i = 0;
	while ((i < n) && (((char *)a)[i] == ((char *)b)[i]))
		i++;
	return (i != n);
}

char	*ft_strdup(const char *s)
{
	size_t	size;
	char	*res;

	res = NULL;
	if (s)
	{
		size = ft_strlen(s) + 1;
		res = malloc(size);
		if (res)
			ft_strcpy(s, res);
	}
	return (res);
}

void	bzero(void *p, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n)
	{
		((char *)p)[i] = 0;
		i++;
	}
}
