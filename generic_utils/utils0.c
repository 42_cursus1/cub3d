/* ************************************************************************** */
/**//**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//**/
/* ************************************************************************** */

#include "../includes/generic_utils.h"

char	*find_chr(char *s, char c)
{
	size_t	i;

	if (!s)
		return (NULL);
	i = 0;
	while (s[i] && (s[i] != c))
		i++;
	return (&(s[i]));
}

void	ft_strcpy(const char *src, char *dst)
{
	size_t	i;

	if (dst)
	{
		i = 0;
		while (src && src[i])
		{
			dst[i] = src[i];
			i++;
		}
		dst[i] = '\0';
	}
}

size_t	ft_strlen(const char *s)
{
	size_t	size;

	size = 0;
	if (s)
	{
		while (s[size])
			size++;
	}
	return (size);
}

void	mmcpy(const void *src, void *dst, size_t n)
{
	size_t	i;

	if (src && dst && n)
	{
		i = 0;
		while (i < n)
		{
			((char *)dst)[i] = ((char *)src)[i];
			i++;
		}
	}
}

void	mmove(const void *src, void *dst, size_t n)
{
	if (src < dst)
	{
		while (n > 0)
		{
			((char *)dst)[n - 1] = ((char *)src)[n - 1];
			n--;
		}
		return ;
	}
	mmcpy(src, dst, n);
}
