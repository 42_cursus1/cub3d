/* ************************************************************************** */
/*                                                                            */
/*     ▄▄▄▄███████▄                                         ▄▄▀█▄             */
/*   ▐██   ███▄▄▄███              ▄▄▄                      █▀   █             */
/*   ███████ ▐█████▀          ▄█▀▀▀  ▀▀▀▄                 ▄█   ▄█             */
/*   ▀▀███████████▀ █▄      ▄█▀         █    ███▄   ▄▄▄▄▄ █   █▀              */
/*   █  ██████████  ██▄    ▄▀    ▄▄▄▄▄▄ █  ▄█   █   █   ▀█▀   █               */
/*  ▐█▌ ██████████  ███▄   █    █▀    ▀▀▀  █   ▄█   █▄    █   █               */
/*  ███ ▐█████████  ████   █   █         ▄█  ▄▀▀     █▄   ▀▄  ▀▄▄▄▄▄          */
/*  ███▌ █▀▀██▀▀██ ▐█████  █   █         █   █       ▄█    █       ▀▀█▄       */
/* ▐████   ▄  ▄ ▀█ ▐█████  █▄  ▀█        █▄   ▀▄▄▄▄▄█▀     █ ▄▄█▀▀▄▄  █       */
/* ██████ ██ ▄██   ███████  █   ▀█▄    ▄▄▄█▄▄            ▄█  █▄ ▄▄▄█  █       */
/* █████████████▄ ███████   ▀▄    ▀▀▀▀▀▀  █ ▀▀█▄▄    ▄▄▄██    ▀▀▀     █       */
/* ██████████████████████▌   ▀▄           █      ▀▀▀▀    ▀▀▄▄▄▄▄▄▄▄▄▄█        */
/* ▀▀▀▀██████████████▀▀▀▀      ▀█▄▄▄▄▄▄▄▄█                                    */
/*                                                                            */
/**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//*    */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H

# include <fcntl.h>
# include <stdio.h>
# include <errno.h>
# include <math.h>
# include <string.h>
# include <stdint.h>
# include <stdbool.h>

# include "gnl.h"
# include "mlx.h"

# define X_RESOLUTION 1920
# define Y_RESOLUTION 1010

# define TEX_SIZE	128
# define SQUARE_2D	30

# ifndef CUB3D_BONUS
#  define CUB3D_BONUS 0
# endif

typedef struct s_img
{
	void		*img;
	char		*addr;
	int			bits_per_pixel;
	int			bytes_per_pixel;
	int			line_length;
	int			endian;
	int			szx;
	int			szy;
}	t_img;

typedef struct s_graph
{
	void	*mlx_ptr;
	void	*frame;
	void	*window;
	void	*texture_north;
	void	*texture_south;
	void	*texture_east;
	void	*texture_west;
	int		color_floor;
	int		color_ceiling;
}	t_graph;

/* texture_* type change during execution:
 *	before load_textures(): pointer to the paths of the texture file (char*)
 *	after load_textures(): pointer to t_img (t_img*)
 */

typedef struct s_map
{
	size_t	number_of_lines;
	size_t	*line_sizes;
	char	**cells;
}	t_map;

typedef enum e_cell_type
{
	SPACE,
	WALL,
	OUT
}	t_celltype;

typedef struct s_player
{
	double			pos_x;
	double			pos_y;
	double			dir_x;
	double			dir_y;
	double			plane_x;
	double			plane_y;
	unsigned int	map_x;
	unsigned int	map_y;
	double			deltadist_x;
	double			deltadist_y;
	double			raydir_x;
	double			raydir_y;
	double			sidedist_x;
	double			sidedist_y;
	double			perpwalldist;
	double			step_x;
	double			step_y;
	int				side;
	int				tex_x;
	int				tex_y;
	double			step;
	double			tex_pos;
	unsigned int	line_height;
	unsigned int	draw_start;
	unsigned int	draw_end;
}	t_player;

typedef uint8_t	t_input;

typedef struct s_game
{
	t_graph		graphics;
	t_map		map;
	t_player	player;
	t_input		input;
}	t_game;

/* images.c */
void		mlx_img_to_img(void *raw, t_img *img);
void		destroy_img(void *mlx, t_img *img);
void		push_frame_to_win(t_graph *graphics);
int			get_pixel_color(t_img *img, int x, int y);
void		my_mlx_pixel_put(t_img *data, int x, int y, int color);

/* map_checking.c */
t_celltype	get_cell_type(t_map *map, int x, int y);

/* free_structs.c */
void		free_graphics(t_graph *graphics);
void		free_map(t_map *map);

/* init.c */
int			init_cub3d(char *path, t_map *map, t_graph *graphics,
				t_player *player);

/* cub3d.c */
int			cub3d(t_game *game);

/* hooks_and_input.c */
void		hooks_setup(t_game *game);

//void		ray_length(t_player *player);

/* raycasting.c */
void		vector_init(t_player *player, int i);
void		stepper(t_player *player);
void		dda(t_player *player, t_map *map);
void		computing_ray_distance(t_player *player, int tex_size);

/* rendering.c */
int			render(t_map *map, t_graph *graphics, t_player *player);

/* drawing.c */
void		draw_column(t_player *player, t_graph *graphics, int i,
				t_img *texture);

#endif
