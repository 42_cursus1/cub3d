/* ************************************************************************** */
/**//**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//**/
/* ************************************************************************** */

#ifndef GENERIC_UTILS_H
# define GENERIC_UTILS_H
# include <stdlib.h>
# include <stdbool.h>

/* utils0.c */
char	*find_chr(char *s, char c);
void	ft_strcpy(const char *src, char *dst);
size_t	ft_strlen(const char *s);
void	mmcpy(const void *src, void *dst, size_t n);
void	mmove(const void *src, void *dst, size_t n);

/* utils1.c */
int		ft_strcmp(const char *a, const char *b);
char	*skip_spaces(char *ptr);
int		mmcmp(const void *a, const void *b, size_t n);
char	*ft_strdup(const char *s);
void	bzero(void *p, size_t n);

/* utils2.c */
size_t	find_odd_one_out(const char *s, const char *charset);
size_t	count_char_occurences(const char *s, const char c);
int		one_or_no_char_occurence(const char *s, const char *charset);

#endif
