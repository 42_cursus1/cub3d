/* ************************************************************************** */
/**//**//*   *//**//*   By: *//**//*   Created:   by *//*   Updated:   by *//**/
/* ************************************************************************** */

#ifndef GNL_H
# define GNL_H
# include <stdlib.h>
# include <unistd.h>
# include <limits.h>

# include "generic_utils.h"

# ifndef GNL_BUFFER_SIZE
#  define GNL_BUFFER_SIZE 64
# endif

typedef struct s_remaining_bytes_per_fd
{
	int								fd;
	char							remaining_bytes[GNL_BUFFER_SIZE + 1];
	struct s_remaining_bytes_per_fd	*next;
}	t_rbpf;

/* remains.c */
char		*get_remains(t_rbpf **remains, int fd);
void		remove_fd_remain(t_rbpf **remains, int fd);
void		free_all_remains(t_rbpf **remains);

/* get_next_line.c */
long int	get_next_line(char **line, int fd);
/*
 * to free all remains:		get_next_line(NULL, -1)
 * to free the remains of 'fd':	get_next_line(NULL, fd)
 */

#endif
