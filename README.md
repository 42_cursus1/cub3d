# cub3D


## Description
In this group project, we want to create a 3d environment in which the player can move around, much like Wolfenstein 3D : we will do so by implementing a raycast algorithm.

## Installation, usage
Use make, then launch the program with a map :
./cub3D <path_to_map>
a few maps are provided in the 'maps' directory, as well as a few textures. Be warned : the larger the textures, the longer the game-loading time...




